from typing import List
from re import split
import sys
import os
import multiprocessing
from bin.util import filter_comments
from bin.downloader import extract_playlist_info, download
import bin.opts as opts
import bin.cmdparser as cmdparser

'''
TO DO:
    create function to read and store account and passwords by websites
    regex to identify youtube urls and playlists
    function to identify if there are subtitles for the video
'''

def main():
    prog_name = sys.argv[0]
    
    parser = cmdparser.create_argparser()

    args = vars(parser.parse_args())

    if not args['account'] and not args['cookies']:
        args['browser'] = [opts.DEFAULT_OPTS['BROWSER']] #  FLAG --cookies-from-browser needs an array of strings, else it will parse as if the str is an array itself

    if args['extract']:
        video_list, playlist_name = extract_playlist_info(args['extract'], args['cookies'])
        playlist_file = "playlist_"+playlist_name+".txt"
        output_file = open(playlist_file, 'w', encoding="utf-8") #  encoding needed for some special characters in videos titles
        for line in video_list:
            output_file.writelines(line+"\n")
        output_file.close()
        print("Finished extracting playlist info to {}".format(playlist_file))
    
    # Testing if the URL list is empty
    file_size = 0
    try: 
        file_size = os.stat(args['list']).st_size
    except FileNotFoundError:
        print("File with webpage urls '{}' not found".format(opts.DEFAULT_OPTS['LIST']))
        print(f"Run script with '{prog_name} -h' for more details.")
        return 1

    if file_size > 0:

        list_file = open(args['list'], 'r')
        listLines = list_file.readlines()
        list_file.close()
        filtered_list = filter_comments(listLines)
        if args['media'] == 'audio' and args['folder'] == opts.DEFAULT_OPTS['VIDEO_FOLDER']:
            # If the selected format is audio and the user didnt specify any output folder
            args['folder'] = opts.DEFAULT_OPTS['AUDIO_FOLDER']
        print("Executing. Run script with '{} -h' for more details.".format(prog_name))

        if args['process'] > 1:
            # https://miguendes.me/how-to-pass-multiple-arguments-to-a-map-function-in-python
            items = [(args, line) for line in filtered_list] # same args for each tuple, but different urls
            pool = multiprocessing.Pool(processes=args['process'])
            pool.starmap(download,items) # starmap iterates over each tuple in items
        else:
            for url in filtered_list:
                download(args, url)
    return 0

if __name__ == '__main__':
    main()