TYPES = ['yt_dlp', 'youtube_dl']
FORMATS = ['video','audio']
BROWSERS = ['chrome', 'firefox', 'edge', 'brave', 'opera', 'chromium', 'safari', 'vivaldi']
N_PROCESS = [x for x in range(1,9)]

DEFAULT_OPTS = {
    'TYPE' : TYPES[0],
    'FORMAT' : FORMATS[0],
    'BROWSER' : BROWSERS[0],
    'PROCESS' : N_PROCESS[1],
    'INPUT_FILE' : 'list.txt',
    'VIDEO_FOLDER' : 'videos/',
    'AUDIO_FOLDER' : 'audio/'
}