import youtube_dl
from youtube_dl.YoutubeDL import YoutubeDL
import yt_dlp #  youtube_dl is having problems with download speed and age restricted youtube videos, yt_dlp however works fine
import traceback
import urllib.error
from .util import remove_starting_whitespaces
from . import opts

def create_downloader(ytdl_opts:dict=None, ytdl_type:str='yt_dlp') -> YoutubeDL:
    if ytdl_type == 'youtube_dl':
        return youtube_dl.YoutubeDL(ytdl_opts)
    return yt_dlp.YoutubeDL(ytdl_opts)

def extract_playlist_info(playlist_url:str, cookies_file:str=None):
    '''
    Outputs list with the URL, uploader's name and video name from each video in a playlist.
    '''
    video_list = []
    ydl_options = {}
    if cookies_file:
        ydl_options['cookiefile'] = cookies_file
    ydl = create_downloader(ydl_options)
    playlist = ydl.extract_info(playlist_url, download=False)
    playlist_name = None

    if 'entries' in playlist: #  if the returned dictionary has 'entries' key, then its a playlist
        playlist_name = playlist['entries'][0]['playlist']
        video_list.append("# playlist '{}'".format(playlist_name))
        
        # for i, entry in enumerate(playlist['entries']):
        for i in range(len(playlist['entries'])):
            video_info = "# "+playlist['entries'][i]['title'] + " by " + playlist['entries'][i]['uploader']
            video_list.append(video_info)
            video_list.append(playlist['entries'][i]['webpage_url'])
    
    return video_list, playlist_name

def download(args:dict, url_line:str):
    """
    Downloads one file only. Adapted for multiprocessing usage.
    """
    cookies_file = args['cookies']
    acc_file = args['account'] # WIP
    ytdl_type = args['downloader']

    ytdl_type = ytdl_type.lower()
    
    print("Downloading with {}".format(ytdl_type))

    # array to check if a url is from youtube
    youtube_urls = ['https://www.youtube.com/', 'https://youtu.be/']
    
    # account file loop
    if acc_file: #  both youtube_dl and yt_dlp are having problems with account login, using --cookies flag instead
        file = open(acc_file, 'r')
        file_lines = file.readlines()
        for line in file_lines:
            website, account, password = line.rstrip().split(',')
            if website == 'youtube':
                default_youtube_acc = account
                default_youtube_pass = password
    
    outputPATH = args['folder']
    if args['subfolder'] is not None:
        outputPATH += args['subfolder'] + '/'

    formatOPT = 'bestaudio'
    if args['media'] == opts.DEFAULT_OPTS['FORMAT']:
        formatOPT = 'bestvideo+'+formatOPT
    outtmplOPT = '[%(uploader)s] - %(upload_date)s - %(title)s.%(ext)s'
    postprocessorsOPT = [ {'key': 'FFmpegExtractAudio', 'preferredcodec': 'mp3', 'preferredquality': '192'} ]
    writesubtitlesOPT = False #  TO DO
    embedsubtitlesOPT = False #  TO DO
    
    # Default options
    default_ydl_opts = {
        'format': formatOPT,
        'outtmpl': outputPATH + outtmplOPT,
    }
    ydl_opts = default_ydl_opts
    # Options by output type
    if args['media'] == opts.DEFAULT_OPTS['FORMAT']:
        ydl_opts['writesubtitles'] = writesubtitlesOPT
        ydl_opts['embedsubtitles'] = embedsubtitlesOPT
    #if args['media'] == FORMATS[1]:
    else:
        ydl_opts['postprocessors'] = postprocessorsOPT

    
    playlist_name = None
    
    # Acquiring URL and password
    try:
        videoURL, videoPWD = url_line.rstrip().split(',')
        videoURL = remove_starting_whitespaces(videoURL)
        videoPWD = remove_starting_whitespaces(videoPWD)
        if videoPWD.upper() == "NONE":
            videoPWD = None
    except:
        videoURL= url_line.rstrip()
        videoPWD = None

    if 'playlist' in videoURL:
        _, playlist_name = extract_playlist_info(videoURL, cookies_file)
        ydl_opts['outtmpl'] = outputPATH + playlist_name + '/' + outtmplOPT
    else:
        ydl_opts['outtmpl'] = outputPATH + outtmplOPT

    # Login options
    if acc_file:
        for url in youtube_urls:
            if url in videoURL:
                ydl_opts['username'] = default_youtube_acc
                ydl_opts['password'] = default_youtube_pass
    elif cookies_file:
        ydl_opts['cookiefile'] = cookies_file
    elif ytdl_type == opts.DEFAULT_OPTS['TYPE']:
        ydl_opts['cookiesfrombrowser'] = args['browser']

    if videoPWD:
        ydl_opts['videopassword'] = videoPWD

    # print(ydl_opts)

    ydl = create_downloader(ydl_opts, ytdl_type)

    retcode = 1 #  0 means that no error ocurred
    tries = 1
    max_tries = 3

    bunkr_urls = ['https://stream.bunkr.is/d/','https://media-files.bunkr.is/']
    bunkr_urls_to_substitute = ['https://stream.bunkr.is/v/', 'https://cdn.bunkr.is/']
    bunkr_prefix = 'https://media-files'
    bunker_suffix = '.bunkr.is/'
    n=0
    bunkr_n = '' #  necessary for finding out which numbered url the file is stored, assuming no number through 9

    while retcode == 1 and tries <= max_tries and n <= 9:
        try:
            newVideoURL = videoURL

            for url in bunkr_urls_to_substitute:
                if url in videoURL:
                    # videoURL = bunkr_urls[1]+videoURL[len(url):]
                    newVideoURL = bunkr_prefix + bunkr_n + bunker_suffix + videoURL[len(url):]
                    video_format = videoURL[videoURL.rfind('.')+1:] # rfind returns last ocurrence of string
                    ydl_opts['format'] = video_format
            
            retcode = ydl.download([newVideoURL])

        # except (youtube_dl.utils.UnavailableVideoError, yt_dlp.utils.UnavailableVideoError):
        except urllib.error.URLError as e: # Usually occurs when downloading from bunkr
            # print(traceback.format_exc())
            n = n+1
            bunkr_n = str(n)
            max_tries -= 1 #  we dont want to finish iterating before trying every numbered url, so we compensate

        except (yt_dlp.utils.DownloadError, youtube_dl.utils.DownloadError) as e:
            # print(traceback.format_exc())
            if 'Requested format is not available' in e.msg:
                print(f'Trying with "best" format.')
                ydl_opts['format'] = 'best'
            
            ydl = create_downloader(ydl_opts, ytdl_type)
        max_tries += 1