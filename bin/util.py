from typing import List

def filter_comments(orig_list:List[str]) -> List[str]:
    '''
    Ignores lines starting with '#' in a list 
    '''
    filtered_list = []
    for line in orig_list:
        if line[0] != '#' and line[0] != "#":
            filtered_list.append(line)
    return filtered_list

def remove_starting_whitespaces(string:str) -> str:
    '''
    Returns string starting from the first ocurrence of non whitespace or non Zero Width Non Joiner character
    '''
    new_string = string
    while ascii(new_string[0]) == 8204 or new_string[0] == " ": # while the first character is the Zero Width Non Joiner or a whitespace
        new_string = new_string[1::] # then it is removed (password receives itself starting from position 1, or videoPWD[1:len(password)-1])
    return new_string

def alter_bunkr_url(url:str) -> str:
    '''
    WIP
    '''
    # bunkr_urls = ['https://stream.bunkr.is/d/','https://media-files.bunkr.is/']
    # bunkr_urls_to_substitute = ['https://stream.bunkr.is/v/', 'https://cdn.bunkr.is/']
    bunkr_urls = ['https://stream.bunkr.is/','https://cdn.bunkr.is/']
    bunkr_url_download = 'https://media-files4.bunkr.is/'
    new_url = ''