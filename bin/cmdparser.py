import argparse
from . import opts

def create_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description='Download videos or music in batch, with options of using an existing account or video password')
    parser.add_argument(
        '-d','--downloader', 
        type=str, 
        choices=opts.TYPES, 
        default=opts.DEFAULT_OPTS['TYPE'], 
        help=f'Downloader manager, {opts.DEFAULT_OPTS["TYPE"]} by default.'
    )
    parser.add_argument(
        '-m','--media',
        type=str,
        choices=opts.FORMATS,
        default=opts.DEFAULT_OPTS["FORMAT"],
        help=f'Media output, {opts.DEFAULT_OPTS["FORMAT"]} by default.'
    )
    parser.add_argument(
        '-l','--list', 
        type=str,
        metavar='LIST_PATH', 
        default=opts.DEFAULT_OPTS["INPUT_FILE"], 
        help=f'Path to file with list of URLs, {opts.DEFAULT_OPTS["INPUT_FILE"]} by default'
    )
    parser.add_argument(
        '--extract',
        type=str,
        metavar='URL',
        default=None,
        help='Playlist url to extract info from.'
    )
    parser.add_argument(
        '-F','--folder',
        type=str,
        metavar='OUTPUT_PATH',
        default=opts.DEFAULT_OPTS["VIDEO_FOLDER"],
        help='Path to destination folder.'
    )
    parser.add_argument(
        '-p','--process',
        type=int,
        choices=opts.N_PROCESS,
        default=opts.DEFAULT_OPTS["PROCESS"],
        help=f'If multiprocess download is used. {opts.DEFAULT_OPTS["PROCESS"]} process by default.'
    )
    parser.add_argument(
        '--browser', 
        type=str,
        choices=opts.BROWSERS,
        default=opts.DEFAULT_OPTS["BROWSER"],
        help=f'Browser to download cookies from. *{opts.DEFAULT_OPTS["BROWSER"]}* by default. yt-dlp only.'
    )
    parser.add_argument(
        '--subfolder',
        type=str,
        default=None,
        help=f'Name for a new folder to be created in the previously informed output path. If not informed, no aditional folder will be created.'
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        '-a','--account', 
        type=str,
        metavar='ACC_FILE_PATH',
        help='Path to file with account and password to supported websites.'
    )
    group.add_argument(
        '--cookies', 
        type=str,
        metavar='COOKIES_FILE_PATH',
        help=f'Path to cookies file, in NetScape/Mozilla format.'
    )

    return parser